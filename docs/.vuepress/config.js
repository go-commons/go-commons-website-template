module.exports = {
  title: "Groups Portal Template",
  dest: "public",
  base: "/go-commons-website-template/",
  theme: "vuepress-theme-gocommons",

  themeConfig: {
    projectLogo: "/logo.png",
    projectTitle: "Hello World"
  }
};
